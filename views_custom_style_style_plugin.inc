<?php
/**
 * @file
 * The main custom style class plugin.
 */

/**
 * The Views Custom Style class plugin.
 */
class views_custom_style_style_plugin extends views_plugin_style {
  /**
   * Add our option definitions.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['custom-class-firstlast'] = array('default' => 0);
    $options['custom-class-columncount'] = array('default' => 0);
    $options['custom-class-columncount-prefix'] = array('default' => 'col');

    return $options;
  }

  /**
   * Add our options to the form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['custom-class-firstlast'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add first/last classes'),
      '#default_value' => $this->options['custom-class-firstlast'],
    );

    $col_options = array();
    for ($i = 0; $i <= 20; $i++) {
      if (!$i) {
        $col_options[$i] = t('<disabled>');
      }
      elseif ($i > 1) {
        $col_options[$i] = $i;
      }
    }

    $form['custom-class-columncount'] = array(
      '#type' => 'select',
      '#title' => t('Add column count classes'),
      '#options' => $col_options,
      '#default_value' => $this->options['custom-class-columncount'],
    );

    $form['custom-class-columncount-prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Column count class prefix'),
      '#default_value' => $this->options['custom-class-columncount-prefix'],
      '#size' => 20,
    );
  }

}
