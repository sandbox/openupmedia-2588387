<?php
/**
 * @file
 * Views Custom Style plugin.
 */

// Define the module path.
define('VIEWS_CUSTOM_STYLE_PATH', drupal_get_path('module', 'views_custom_style'));
// Define the js path.
define('VIEWS_CUSTOM_STYLE_PATH_JS', VIEWS_CUSTOM_STYLE_PATH . '/assets/js/');
// Define the css path.
define('VIEWS_CUSTOM_STYLE_PATH_CSS', VIEWS_CUSTOM_STYLE_PATH . '/assets/css/');

/**
 * Implements hook_views_api().
 */
function views_custom_style_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Theme preprocess function for views-view-custom.tpl.php.
 */
function views_custom_style_preprocess_views_view_custom(&$vars) {
  $view = $vars['view'];
  $vars['rows'] = $view->style_plugin->rendered_fields;

  $count = count($vars['rows']);
  for ($i = 0; $i < $count; $i++) {
    // Initialize classes to be added.
    $class = array();

    // First and last record.
    if ($vars['options']['custom-class-firstlast']) {
      if ($i == 0) {
        array_push($class, 'first');
      }
      elseif ($i == $count - 1) {
        array_push($class, 'last');
      }
    }

    // Column counter.
    if ($vars['options']['custom-class-columncount'] && drupal_strlen($vars['options']['custom-class-columncount-prefix'])) {
      array_push($class, $vars['options']['custom-class-columncount-prefix'] . (($i % $vars['options']['custom-class-columncount']) + 1));
    }

    // Add the classes.
    $vars['rows'][$i]['classes'] = implode(' ', $class);

    if (isset($view->result[$i]->nid)) {
      // Add the url towards the node.
      $vars['rows'][$i]['url'] = url('node/' . $view->result[$i]->nid);

      // Add the node id.
      $vars['rows'][$i]['nid'] = $view->result[$i]->nid;

      // Add previous/next nids and urls.
      if ($count > 1) {
        $vars['rows'][$i]['nid_previous'] = ($i == 0) ? $view->result[$count - 1]->nid : $view->result[$i - 1]->nid;
        $vars['rows'][$i]['url_previous'] = ($i == 0) ? url('node/' . $view->result[$count - 1]->nid) : url('node/' . $view->result[$i - 1]->nid);
        $vars['rows'][$i]['nid_next'] = ($i == ($count - 1)) ? $view->result[0]->nid : $view->result[$i + 1]->nid;
        $vars['rows'][$i]['url_next'] = ($i == ($count - 1)) ? url('node/' . $view->result[0]->nid) : url('node/' . $view->result[$i + 1]->nid);
      }
    }
  }

  // Draggable views implementation.
  // Check whether this table view has draggableview field.
  if (!isset($vars['view']->field['draggableviews'])) {
    return;
  }

  // Check permissions.
  if (!user_access('access draggableviews')) {
    return;
  }

  // Add class to ul item of the view.
  $vars['orderWrapperClass'] = 'views_custom_style-grid-' . rand(0, 1000);

  // Add javascript.
  drupal_add_library('system', 'ui.sortable');

  // Add setting of the row class.
  $js_setting = array(
    'views_custom_style_order_class' => $vars['orderWrapperClass'],
  );
  if ($vars['options']['custom-class-columncount'] && drupal_strlen($vars['options']['custom-class-columncount-prefix'])) {
    $js_setting['views_custom_style_order_col_prefix'] = $vars['options']['custom-class-columncount-prefix'];
    $js_setting['views_custom_style_order_col_count'] = $vars['options']['custom-class-columncount'];
  }
  drupal_add_js($js_setting, 'setting');

  // Add custom js and css.
  drupal_add_js(VIEWS_CUSTOM_STYLE_PATH_JS . 'views_custom_style_order.js');
  drupal_add_css(VIEWS_CUSTOM_STYLE_PATH_CSS . 'views_custom_style_order.css');
}
