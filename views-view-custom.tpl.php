<?php
/**
 * @file
 * The default Views Custom Style template.
 *
 * Available variables:
 * - $rows: the result of the view as an array.
 */
?>
<?php foreach($rows as $index => $row): ?>
	<?php foreach($row as $index => $field): ?>
		<?php print $field . '<br />'; ?>
	<?php endforeach; ?>
	<?php print '<br />'; ?>
<?php endforeach; ?>
