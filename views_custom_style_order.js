/**
 * @file
 * Adds draggable functionality to the html list display of the view.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.views_custom_style_order = {
    attach: function (context, settings) {
      var $el = $('.views-form .' + Drupal.settings.views_custom_style_order_class + ':not(.draggableviews-processed)', context);

      var updateColumns = function () {
        if (Drupal.settings.views_custom_style_order_col_count) {
          $el.children(':not(.ui-sortable-helper)').each(function (i) {
            var $this = $(this);
            var classname = '';

            for (var j = 0; j <= Drupal.settings.views_custom_style_order_col_count; j++) {
              $this.removeClass(Drupal.settings.views_custom_style_order_col_prefix + j);
            }

            classname = Drupal.settings.views_custom_style_order_col_prefix + ((i % Drupal.settings.views_custom_style_order_col_count) + 1);
            $this.addClass(classname);
          });
        }
      };

      var sortableOptions = {
        tolerance: 'intersect',
        scroll: true,
        revert: true,
        forcePlaceholderSize: true,
        cursor: 'move',
        change: function (event, ui) {
          updateColumns();
        },
        update: function (event, ui) {
          $(".draggableviews-weight").each(function (i, Val) {
            $(this).val(i);
          });

          if (!$(this).hasClass('draggableviews-changed')) {
            $('<div class="draggableviews-changed-warning messages warning">' + Drupal.t('Changes made in this list will not be saved until the form is submitted.') + '</div>')
              .insertBefore($(this).parents('form div')).hide().fadeIn('slow');
            $(this).addClass('draggableviews-changed');
          }

          // If Ajax enabled, we should submit the form.
          if (Drupal.settings.draggableviews_ajax) {
            $(this).parent().parent().find('#edit-submit').trigger('mousedown');
          }

          updateColumns();
        }
      };

      // Add sortable effect.
      $el.sortable(sortableOptions);
      // Add class for theming.
      $el.addClass('draggableviews-processed');

      if (Drupal.settings.draggableviews_ajax) {
        $('.views-form .' + Drupal.settings.draggableviews_row_class).parent().parent().find('#edit-submit').hide();
      }
    }
  };
})(jQuery);
