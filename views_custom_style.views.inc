<?php
/**
 * @file
 * Defines the Views plugin for the Views Custom Style module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_custom_style_views_plugins() {
  return array(
    'style' => array(
      'custom_style' => array(
        'title' => t('Custom Style'),
        'help' => t('Add a custom style with only 1 template file'),
        'handler' => 'views_custom_style_style_plugin',
        'theme' => 'views_view_custom',
        'uses row plugin' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
