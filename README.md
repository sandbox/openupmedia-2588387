INTRODUCTION
------------

Views Custom Style is targeted at themers. It allows you to limit the amount
of custom template files for your Displays. It also adds more flexibility as
all resulting rows and fields can be printed within one template file. Only 2
templates remain, the wrapper (views-view.tpl.php) and the custom style wrapper
(views-view-custom.tpl.php).


REQUIREMENTS
------------

This module requires the following modules:
 * Views (http://drupal.org/project/views)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------

After enabling the module a new Views Style plugin will become available. You
can select it under the Format section when creating or editing a View.


MAINTAINERS
-----------

Current maintainers:
 * El Phaenax (elphaenax) - https://www.drupal.org/u/elphaenax

The project has been created at and sponsored by:
 * Open Up Media
   Open Up Media is a full service brand agency, based in Antwerp, Belgium. The
   agency originated from the merger between Savant Media and Apluz. Both
   agencies brought their years of experience and expertise together to offer
   customers a wider range of services.

   We have been developing websites almost exclusively using Drupal since 2007.
   We have a complimentary team that delivers beautiful, user-friendly, and
   rock-solid websites. Visit https://www.openupmedia.be for more information.
